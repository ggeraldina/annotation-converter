# Конвертация

Возможные варианты:
* INTERNAL -> INTERNAL_CSV
* INTERNAL_CSV -> PASCAL_VOC
* PASCAL_VOC -> INTERNAL_CSV
* INTERNAL_CSV -> INTERNAL


# Создание образа

```
$docker build -t python-converter:1.0 .
```

# Запуск контейнера

## Без переопределения переменных окружения

По умолчанию:

IN_DATASET_FORMAT="INTERNAL"

OUT_DATASET_FORMAT="INTERNAL_CSV"

IN_DATASET_PATH="$WORKDIR/data/json_ann"

OUT_DATASET_PATH="$WORKDIR/data/csv_ann"  



```
$docker run \
-it \
-v /home/geraldina/Program/Python/ori_test/data:$WORKDIR/data \
--rm \
--name test-converter python-converter:1.0
```

## С переопределением переменных окружения

```
$docker run \
-it \
-v /home/geraldina/Program/Python/ori_test/data:$WORKDIR/data \
-e IN_DATASET_FORMAT="INTERNAL_CSV" \
-e OUT_DATASET_FORMAT="PASCAL_VOC" \
-e IN_DATASET_PATH="$WORKDIR/data/csv_ann" \
-e OUT_DATASET_PATH="$WORKDIR/data/xml_ann" \
--rm \
--name test-converter python-converter:1.0
```


