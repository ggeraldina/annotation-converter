import os
from dotenv import load_dotenv

from json_to_csv import json_to_csv
from csv_to_pascal_voc import csv_to_pascal_voc
from pascal_voc_to_csv import pascal_voc_to_csv
from csv_to_json import csv_to_json


# Загрузка переменных среды из файла
load_dotenv()

# Чтение переменных среды
in_dataset_format = os.getenv('IN_DATASET_FORMAT')
out_dataset_format = os.getenv('OUT_DATASET_FORMAT')
in_dataset_path = os.path.abspath(os.getenv('IN_DATASET_PATH'))
out_dataset_path = os.path.abspath(os.getenv('OUT_DATASET_PATH'))

# Вызов соответствующей функции
if (
    in_dataset_format == "INTERNAL"
    and out_dataset_format == "INTERNAL_CSV"
):
    json_to_csv(in_dataset_path, out_dataset_path)
elif (
    in_dataset_format == "INTERNAL_CSV"
    and out_dataset_format == "PASCAL_VOC"
):
    csv_to_pascal_voc(in_dataset_path, out_dataset_path)
elif (
    in_dataset_format == "PASCAL_VOC"
    and out_dataset_format == "INTERNAL_CSV"
):
    pascal_voc_to_csv(in_dataset_path, out_dataset_path)
elif (
    in_dataset_format == "INTERNAL_CSV"
    and out_dataset_format == "INTERNAL"
):
    csv_to_json(in_dataset_path, out_dataset_path)
else:
    print(
        """ 
        Невозможно конвертировать.

        Возможные варианты:
        INTERNAL -> INTERNAL_CSV
        INTERNAL_CSV -> PASCAL_VOC
        PASCAL_VOC -> INTERNAL_CSV
        INTERNAL_CSV -> INTERNAL
        """
    )
