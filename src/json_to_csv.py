import csv
import json
import os

from PIL import Image
from tqdm import tqdm

from utils import parse, print_conversion_message, create_new_images_dir, copy_image
from constants import CSV_FIELDNAMES, IMAGES_DIR, JSON_MARKUP_DIR, MARKUP_CSV


@print_conversion_message("INTERNAL -> INTERNAL_CSV")
def json_to_csv(in_ann_path, out_ann_path):
    """Конвертация датасета из формата Internal в формат Internal CSV

    Args:
        in_ann_path (str): Путь к папке с датасетом в формате Internal
        out_ann_path (str): Путь к папке с датасетом в формате Internal CSV format
    """
    try:
        create_new_images_dir(out_ann_path)
        csv_file_path = os.path.join(out_ann_path, MARKUP_CSV)
        with open(csv_file_path, "w", newline="") as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=CSV_FIELDNAMES)
            writer.writeheader()
            images_files = os.listdir(os.path.join(in_ann_path, IMAGES_DIR))
            for image_file in tqdm(images_files):
                ann_path, ann_exist = _get_ann_path(in_ann_path, image_file)
                if not ann_exist:
                    continue
                width, height = _get_width_height_images(in_ann_path, image_file)
                copy_image(in_ann_path, out_ann_path, image_file)
                with open(ann_path) as json_file:
                    data = json.load(json_file)
                    try:
                        _write_csv_row(writer, image_file, data, width, height)                    
                    except KeyError as e:
                        print(f"Неверный файл разметки {image_file}. \nОшибка: KeyError {e}")
                    except ValueError as e:
                        print(f"Неверный файл разметки {image_file}. \nОшибка: ValueError \n{e}")
    except FileNotFoundError as e:
        print(f"Не удается найти путь. \nОшибка:{e}")

def _get_width_height_images(in_ann_path, image_file):
    """Определить размер изображения

    Args:
        in_ann_path (str): Путь к папке с датасетом в формате Internal
        image_file (str): Название изображения с форматом

    Returns:
        int, int: width, height - ширина и высота изображения
    """
    image = Image.open(os.path.join(in_ann_path, IMAGES_DIR, image_file))
    width, height = image.size
    image.close()
    return width, height

def _get_ann_path(in_ann_path, image_file):
    """Получить абсолютный путь для исходной разметки

    Args:
        in_ann_path (str): Путь к папке с датасетом в формате Internal
        image_file (str): Название изображения с форматом

    Returns:
        str, bool: ann_path, ann_exist - Путь к разметке, существование файла
    """
    ann_exist = True
    ann_name = image_file.split(".")[0]
    ann_path = os.path.join(in_ann_path, JSON_MARKUP_DIR, f"{ann_name}.json")
    if not os.path.exists(ann_path):
        print(f"\nОтсутствует аннотация для изображения {image_file}")
        ann_exist = False
    return ann_path, ann_exist

def _write_csv_row(writer, image_file, data, width, height):
    """Записать строку с разметкой в CSV

    Args:
        writer (csv.DictWriter): Объект writer
        image_file (str): Название изображения с форматом
        data (Dict): Словарь с разметкой
        width (int): Ширина изображения
        height (int): Высота изображения
    """
    for box_ann in data:
        xmin, xmax, ymin, ymax = _get_xmin_xmax_ymin_ymax(box_ann)
        writer.writerow(
            {
                "filename": f"images/{image_file}",
                "width": width,
                "height": height,
                "class": box_ann["label"],
                "xmin": xmin,
                "ymin": ymin,
                "xmax": xmax,
                "ymax": ymax
            }
        )

def _get_xmin_xmax_ymin_ymax(box_ann):
    """Получить xmin, xmax, ymin, ymax из записи json-аннотации

    Args:
        box_ann (dict): Словарь с координатами ограничительной рамки и 
        меткой класса, где "x","y" - координаты точки начала выделения 
        прямоугольника, "x1","y1" - координаты точки окончания выделения 
        прямоугольника,"label" - класс объекта.

    Returns:
        float, float, float, float: xmin, xmax, ymin, ymax,
        где xmin, ymin - координаты верхней левой точки bounding box,
        xmax, ymax - координаты нижней правой точки bounding box
    """
    x = float(box_ann["x"])
    x1 = float(box_ann["x1"])
    y = float(box_ann["y"])
    y1 = float(box_ann["y1"])
    xmin = x if x < x1 else x1
    xmax = x1 if x < x1 else x
    ymin = y if y < y1 else y1
    ymax = y1 if y < y1 else y
    return xmin, xmax, ymin, ymax


if __name__ == "__main__":
    """Конвертация датасета из формата Internal в формат Internal CSV
    """
    args = vars(parse())
    if not args.get("input", False):
        print("Нужно указать путь к исходной папке. Параметр --input.")
    elif not args.get("output", False):
        print("Нужно указать путь к папке с результатом конвертации. Параметр --output.")
    else:
        json_to_csv(args["input"], args["output"])
