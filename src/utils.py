import argparse
import functools
import os
import shutil

from constants import IMAGES_DIR


def parse():
    """Парсинг аргументов

    Returns:
        argparse.ArgumentParser: Принятые аргументы
    """
    parser = argparse.ArgumentParser(
        description="This example demonstrates converting."
    )
    parser.add_argument(
        "--input", type=str, help="path to input folder"
    )
    parser.add_argument(
        "--output", type=str, help="path to output folder"
    )
    return parser.parse_args()

def print_conversion_message(conversion_type_message):
    """wrapper для вывода сообщения о конвертации

    Args:
        conversion_type_message (str): Сообщение о типе конвертации
    """
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            print(f"\n{conversion_type_message}")    
            print("Началось копирование изображений и конвертация аннотаций")
            func(*args, **kwargs)
            print("Закончилось копирование изображений и конвертация аннотаций")  
            return func
        return wrapper     
    return decorator

def check_image_exist(in_ann_path, image_filename):
    """Проверить существует ли изображение

    Args:
        in_ann_path (str): Путь к папке с входным датасетом
        image_filename (str): Название изображения с форматом

    Returns:
        bool: Существует ли изображение
    """
    image_exist = True
    image_file = os.path.join(in_ann_path, IMAGES_DIR, image_filename)
    if not os.path.exists(image_file):
        print(f"\nОтсутствует изображение {image_file} для аннотации")
        image_exist = False
    return image_exist

def create_new_images_dir(out_ann_path):
    """Создать папку для изображений датасета Internal CSV

    Args:
        out_ann_path (str): Путь к папке с датасетом
    """
    new_images_dir = os.path.join(out_ann_path, IMAGES_DIR)
    if not os.path.exists(new_images_dir):
        os.makedirs(new_images_dir)

def copy_image(in_ann_path, out_ann_path, image_filename):
    """Скопировать изображение

    Args:
        in_ann_path (str): Путь к папке с входным датасетом
        out_ann_path (str): Путь к папке с выходным датасетом
        image_filename (str): Название изображения с форматом
    """
    shutil.copy(
        os.path.join(in_ann_path, IMAGES_DIR, image_filename), 
        os.path.join(out_ann_path, IMAGES_DIR, image_filename)
    )
