MAX_MESSAGE_LENGTH = 80

CSV_FIELDNAMES = [
        "filename",
        "width",
        "height",
        "class",
        "xmin",
        "ymin",
        "xmax",
        "ymax"
    ]

IMAGES_DIR = "images"

JSON_MARKUP_DIR = "markup"
META_JSON = "meta.json"
MARKUP_CSV = "markup.csv"
MARKUP_XML = "markup.xml"
