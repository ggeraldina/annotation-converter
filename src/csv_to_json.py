import csv
import json
import os
import shutil

from utils import parse, print_conversion_message, create_new_images_dir, check_image_exist, copy_image
from constants import IMAGES_DIR, JSON_MARKUP_DIR, MARKUP_CSV, META_JSON, MAX_MESSAGE_LENGTH


@print_conversion_message("INTERNAL_CSV -> INTERNAL")
def csv_to_json(in_ann_path, out_ann_path):
    """Конвертация датасета из формата Internal CSV format в формат Internal

    Args:
        in_ann_path (str): Путь к папке с датасетом в формате Internal CSV format
        out_ann_path (str): Путь к папке с датасетом в формате Internal
    """
    try:            
        create_new_images_dir(out_ann_path)
        _create_new_markup_dir(out_ann_path)
        with open(os.path.join(in_ann_path, MARKUP_CSV), newline="") as csv_file:
            reader = csv.DictReader(csv_file)
            row = next(reader, None)
            last_filename = ""
            labels = set()
            while(True):
                if row is None:
                    break
                image_filename = os.path.basename(row["filename"])
                if not check_image_exist(in_ann_path, image_filename):
                    continue
                copy_image(in_ann_path, out_ann_path, image_filename)            
                print(f"\r{f'Скопирован {image_filename}': <{MAX_MESSAGE_LENGTH}}", end="")
                last_filename, row, labels = _create_ann(
                    out_ann_path, last_filename, reader, row, labels
                )
            with open(os.path.join(out_ann_path, META_JSON), "w") as json_file:
                json.dump({"labels": list(labels)}, json_file)
            # Очистка консоли после окончания работы для корректного 
            # отображения следующего сообщения
            print(f"\r{f' ': <{MAX_MESSAGE_LENGTH}}\r", end="")        
    except FileNotFoundError as e:
        print(f"Не удается найти путь. \nОшибка:{e}")                         
    except KeyError as e:
        print(f"Неверный файл разметки. \nОшибка: KeyError {e}")
    except ValueError as e:
        print(f"Неверный файл разметки. \nОшибка: ValueError \n{e}")
    
def _create_new_markup_dir(out_ann_path):
    """Создать папку для разметки датасета Internal CSV

    Args:
        out_ann_path (str): Путь к папке с новым датасетом
    """
    markup_folder = os.path.join(out_ann_path, JSON_MARKUP_DIR)
    if not os.path.exists(markup_folder):
        os.makedirs(markup_folder)

def _create_ann(out_ann_path, last_filename, reader, row, labels):
    """Создать аннотацию

    Args:
        out_ann_path (str): Путь к папке с новым датасетом
        last_filename (str): Название предыдущего обрабатываемого изображения с форматом
        reader (csv.DictReader): Объект reader
        row (Dict): CSV-строка с разметкой (один ограничивающий прямоугольник)
        labels (Set): Множество из меток класса

    Returns:
        str, Dict, Set: last_filename, row, labels - название текущего 
        обработанного изображения, следующая CSV-строка, множество из меток класса

    """
    first_box = True
    json_data = []
    while(
        not row is None
        and (
            last_filename == os.path.basename(row["filename"])
            or first_box == True
        )
    ):
        first_box = False
        labels.add(row["class"])
        json_data.append(
            {
                "x": float(row["xmin"]),
                "y": float(row["ymin"]),
                "x1": float(row["xmax"]),
                "y1": float(row["ymax"]),
                "label": row["class"]
            }
        )
        last_filename = os.path.basename(row["filename"])
        row = next(reader, None)
    _write_json_ann(out_ann_path, last_filename, json_data)
    return last_filename, row, labels

def _write_json_ann(out_ann_path, last_filename, json_data):
    """Записать аннотацию для изображения

    Args:
        out_ann_path (str): Путь к папке с новым датасетом
        last_filename (str): Название предыдущего обрабатываемого изображения с форматом
        json_data (Dict): Словарь с аннотацией (поля: x, x1, y, y1, label)
    """
    json_name = last_filename.split(".")[0]
    with open(
        os.path.join(out_ann_path, JSON_MARKUP_DIR, f"{json_name}.json"), "w"
    ) as json_file:
        json.dump(json_data, json_file)


if __name__ == "__main__":
    """Конвертация датасета из формата Internal CSV format в формат Internal
    """
    args = vars(parse())
    if not args.get("input", False):
        print("Нужно указать путь к исходной папке. Параметр --input.")
    elif not args.get("output", False):
        print("Нужно указать путь к папке с результатом конвертации. Параметр --output.")
    else:
        csv_to_json(args["input"], args["output"])
