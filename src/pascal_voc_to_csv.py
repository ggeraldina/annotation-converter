import csv
import os
import shutil
import xml.etree.ElementTree as ET

from tqdm import tqdm
from xml.etree.ElementTree import ParseError

from utils import parse, print_conversion_message, create_new_images_dir, check_image_exist, copy_image
from constants import CSV_FIELDNAMES, IMAGES_DIR, MARKUP_CSV, MARKUP_XML


@print_conversion_message("PASCAL_VOC -> INTERNAL_CSV")
def pascal_voc_to_csv(in_ann_path, out_ann_path):
    """Конвертация датасета из формата PascalVOC в формат Internal CSV format

    Args:
        in_ann_path (str): Путь к папке с датасетом в формате PascalVOC
        out_ann_path (str): Путь к папке с датасетом в формате Internal CSV format
    """
    try:            
        create_new_images_dir(out_ann_path)
        csv_file_name = os.path.join(out_ann_path, MARKUP_CSV)
        with open(csv_file_name, "w", newline="") as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=CSV_FIELDNAMES)
            writer.writeheader()
            with open(os.path.join(in_ann_path, MARKUP_XML)) as pascal_voc:
                data = "<root>" + pascal_voc.read() + "</root>"
                root_node = ET.fromstring(data)
                for ann_node in tqdm(root_node.findall("./annotation")):
                    image_filename = ann_node.find('./filename').text
                    if not check_image_exist(in_ann_path, image_filename):
                        continue
                    copy_image(in_ann_path, out_ann_path, image_filename)                
                    _write_csv_row(writer, image_filename, ann_node)    
    except FileNotFoundError as e:
        print(f"Не удается найти путь. \nОшибка:{e}")                         
    except KeyError as e:
        print(f"Неверный файл разметки. \nОшибка: KeyError {e}")
    except ValueError as e:
        print(f"Неверный файл разметки. \nОшибка: ValueError \n{e}")
    except ParseError as e:
        print(f"Неверный файл разметки. \nОшибка: ParseError \n{e}")
    except AttributeError as e:
        print(f"Неверный файл разметки. \nОшибка: AttributeError \n{e}")
        

def _write_csv_row(writer, image_filename, ann_node):
    """Записать строку с разметкой в CSV

    Args:
        writer (csv.DictWriter): Объект writer
        image_filename (str): Название изображения с форматом
        ann_node (ET.SubElement): Тег <annotation>
    """
    filename = os.path.join(
        IMAGES_DIR,
        image_filename
    )
    width = ann_node.find('./size/width').text
    height = ann_node.find('./size/height').text
    for object_node in ann_node.findall("./object"):
        writer.writerow(
            {
                "filename": filename,
                "width": width,
                "height": height,
                "class": object_node.find('name').text,
                "xmin": object_node.find('./bndbox/xmin').text,
                "ymin": object_node.find('./bndbox/ymin').text,
                "xmax": object_node.find('./bndbox/xmax').text,
                "ymax": object_node.find('./bndbox/ymax').text
            }
        )


if __name__ == "__main__":
    """Конвертация датасета из формата PascalVOC в формат Internal CSV format
    """
    args = vars(parse())
    if not args.get("input", False):
        print("Нужно указать путь к исходной папке. Параметр --input.")
    elif not args.get("output", False):
        print("Нужно указать путь к папке с результатом конвертации. Параметр --output.")
    else:
        pascal_voc_to_csv(args["input"], args["output"])
