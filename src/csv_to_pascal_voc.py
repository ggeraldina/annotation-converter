import csv
import os
import shutil
import xml.etree.ElementTree as ET
from xml.dom import minidom

from utils import parse, print_conversion_message, check_image_exist, create_new_images_dir, copy_image
from constants import IMAGES_DIR, MARKUP_CSV, MARKUP_XML, MAX_MESSAGE_LENGTH


@print_conversion_message("INTERNAL_CSV -> PASCAL_VOC")
def csv_to_pascal_voc(in_ann_path, out_ann_path):
    """Конвертация датасета из формата Internal CSV format в формат PascalVOC

    Args:
        in_ann_path (str): Путь к папке с датасетом в формате Internal CSV format
        out_ann_path (str): Путь к папке с датасетом в формате PascalVOC
    """
    try:            
        create_new_images_dir(out_ann_path)
        with open(os.path.join(in_ann_path, MARKUP_CSV), newline="") as csv_file:
            reader = csv.DictReader(csv_file)
            row = next(reader, None)
            result = ""
            last_filename = ""
            while(True):
                if row is None:
                    break
                image_filename = os.path.basename(row["filename"])
                if not check_image_exist(in_ann_path, image_filename):
                    row = next(reader, None)
                    continue
                copy_image(in_ann_path, out_ann_path, image_filename)                        
                print(f"\r{f'Скопирован {image_filename}': <{MAX_MESSAGE_LENGTH}}", end="")
                ann_node, last_filename, row = _create_annotation_node(
                    in_ann_path, last_filename, reader,row
                )                
                result += str(
                    minidom.parseString(
                        ET.tostring(ann_node)
                    ).toprettyxml(indent="   ")
                )
            result = result.replace("<?xml version=\"1.0\" ?>\n", "")
            with open(os.path.join(out_ann_path, MARKUP_XML), "w") as xml_file:
                xml_file.write(result)
            # Очистка консоли после окончания работы для корректного 
            # отображения следующего сообщения
            print(f"\r{f' ': <{MAX_MESSAGE_LENGTH}}\r", end="")    
    except FileNotFoundError as e:
        print(f"Не удается найти путь. \nОшибка:{e}")                         
    except KeyError as e:
        print(f"Неверный файл разметки. \nОшибка: KeyError {e}")
    except ValueError as e:
        print(f"Неверный файл разметки. \nОшибка: ValueError \n{e}")

def _create_annotation_node(in_ann_path, last_filename, reader, row):
    """Создание xml аннотации, тега <annotation>

    Args:
        in_ann_path (str): Путь к папке с датасетом в формате Internal CSV format
        last_filename (str): Название предыдущего обрабатываемого изображения с форматом
        reader (csv.DictReader): Объект reader
        row (Dict): CSV-строка с разметкой (один ограничивающий прямоугольник)

    Returns:
        ET.Element, str, Dict: ann_node, last_filename, row -
        корневой тег аннотации, название текущего обработанного изображения,
        следующая CSV-строка
    """
    ann_node = ET.Element('annotation')
    _create_folder_node(ann_node)
    _create_filename_node(ann_node, row)
    _create_path_node(ann_node, in_ann_path, row)
    _create_source_node(ann_node)
    _create_size_node(ann_node, row)
    _create_segmented_node(ann_node)
    last_filename, row = _create_object_nodes(
        ann_node, 
        last_filename, 
        reader, 
        row
    )
    return ann_node, last_filename, row

def _create_folder_node(ann_node):
    """Создание тега <folder>

    Args:
        ann_node (ET.Element): Родительский тег <annotation>
    """
    folder_node = ET.SubElement(ann_node, 'folder')
    folder_node.text = IMAGES_DIR

def _create_filename_node(ann_node, row):
    """Создание тега <filename>

    Args:
        ann_node (ET.Element): Родительский тег <annotation>
        row (Dict): CSV-строка с разметкой (один ограничивающий прямоугольник)
    """
    filename_node = ET.SubElement(ann_node, 'filename')
    filename_node.text = os.path.basename(row["filename"])

def _create_path_node(ann_node, in_ann_path, row):
    """Создание тега <path>

    Args:
        ann_node (ET.Element): Родительский тег <annotation>
        in_ann_path (str): Путь к папке с датасетом в формате Internal CSV format
        row (Dict): CSV-строка с разметкой (один ограничивающий прямоугольник)
    """
    path_node = ET.SubElement(ann_node, 'path')
    path_node.text = os.path.join(
        in_ann_path,
        os.path.relpath(row["filename"])
    )

def _create_source_node(ann_node):
    """Создание тега <source>

    Args:
        ann_node (ET.Element): Родительский тег <annotation>
    """
    source_node = ET.SubElement(ann_node, 'source')
    database_node = ET.SubElement(source_node, 'database')
    database_node.text = "ORI_Markup"

def _create_size_node(ann_node, row):
    """Создание тега <size>

    Args:
        ann_node (ET.Element): Родительский тег <annotation>
        row (Dict): CSV-строка с разметкой (один ограничивающий прямоугольник)
    """
    size_node = ET.SubElement(ann_node, 'size')
    width_node = ET.SubElement(size_node, 'width')
    width_node.text = row["width"]
    height_node = ET.SubElement(size_node, 'height')
    height_node.text = row["height"]
    depth_node = ET.SubElement(size_node, 'depth')
    depth_node.text = "3"

def _create_segmented_node(ann_node):
    """Создание тега <segmented>

    Args:
        ann_node (ET.Element): Родительский тег <annotation>
    """
    segmented_node = ET.SubElement(ann_node, 'segmented')
    segmented_node.text = "0"

def _create_object_nodes(ann_node, last_filename, reader, row):
    """Создание тегов <object>

    Args:
        ann_node (ET.Element): Родительский тег <annotation>
        last_filename (str): Название предыдущего обрабатываемого изображения с форматом
        reader (csv.DictReader): Объект reader
        row (Dict): CSV-строка с разметкой (один ограничивающий прямоугольник)

    Returns:
        str, Dict: last_filename, row - название текущего обработанного 
        изображения, следующая CSV-строка
    """
    first_box = True
    while(
        not row is None
        and (
            last_filename == os.path.basename(row["filename"])
            or first_box == True
        )
    ):
        first_box = False
        _create_object_node(ann_node, row)
        last_filename = os.path.basename(row["filename"])
        row = next(reader, None)
    return last_filename, row

def _create_object_node(ann_node, row):
    """Создание тега <object>

    Args:
        ann_node (ET.Element): Родительский тег <annotation>
        row (Dict): CSV-строка с разметкой (один ограничивающий прямоугольник)
    """
    object_node = ET.SubElement(ann_node, 'object')
    name_node = ET.SubElement(object_node, 'name')
    name_node.text = row["class"]
    pose_node = ET.SubElement(object_node, 'pose')
    pose_node.text = "Unspecified"
    truncated_node = ET.SubElement(object_node, 'truncated')
    truncated_node.text = "0"
    difficult_node = ET.SubElement(object_node, 'difficult')
    difficult_node.text = "0"
    _create_bndbox_node(object_node, row)

def _create_bndbox_node(object_node, row):
    """Создание тега <bndbox>

    Args:
        object_node (ET.SubElement): Родительский тег <object>
        row (Dict): CSV-строка с разметкой (один ограничивающий прямоугольник)
    """
    bndbox_node = ET.SubElement(object_node, 'bndbox')
    xmin_node = ET.SubElement(bndbox_node, 'xmin')
    xmin_node.text = row["xmin"]
    ymin_node = ET.SubElement(bndbox_node, 'ymin')
    ymin_node.text = row["ymin"]
    xmax_node = ET.SubElement(bndbox_node, 'xmax')
    xmax_node.text = row["xmax"]
    ymax_node = ET.SubElement(bndbox_node, 'ymax')
    ymax_node.text = row["ymax"]


if __name__ == "__main__":
    """Конвертация датасета из формата Internal CSV format в формат PascalVOC
    """
    args = vars(parse())
    if not args.get("input", False):
        print("Нужно указать путь к исходной папке. Параметр --input.")
    elif not args.get("output", False):
        print("Нужно указать путь к папке с результатом конвертации. Параметр --output.")
    else:
        csv_to_pascal_voc(args["input"], args["output"])       
